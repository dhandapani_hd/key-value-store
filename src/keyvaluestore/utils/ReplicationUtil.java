package keyvaluestore.utils;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import keyvaluestore.config.Configuration;

public class ReplicationUtil {
	private final static Logger LOGGER = Logger.getLogger(ReplicationUtil.class.getName());

	public static <K, V> void createReplica(Map<K, V> map, Configuration config, String fileName) {
		int maxReplicas = config.getFileOptions().getMaxBackups();
		while(maxReplicas-- > 0) {
			FileUtil.writeToFile(fileName + maxReplicas, map);
		}
	}

	public static <K, V> Map<K, V> getValidReplica(Configuration config, String fileName){
		int maxReplicas = config.getFileOptions().getMaxBackups();
		while(maxReplicas-- > 0) {
			Map<K, V> map = FileUtil.readFromFile(fileName + maxReplicas);
			if(map != null) {
				return map;
			}
		}
		
		if(maxReplicas > 0) {
			LOGGER.log(Level.SEVERE, "All Replica are corrupted/Deleted");
		}
		
		return null;
	}
}