package keyvaluestore.utils;

public class MemoryUtil {
	public static int getUsedMemoryPercent() {
		long totalMemory = Runtime.getRuntime().totalMemory();
		long usedMemory =  totalMemory - Runtime.getRuntime().freeMemory();
		return (int)((double)usedMemory / (double)totalMemory * 100);
	}
	
	public static double getTimeTaken(long startTime) {
		return (double)(System.currentTimeMillis() - startTime)/(double)1000;
	}
}
