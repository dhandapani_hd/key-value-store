package keyvaluestore.utils;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import keyvaluestore.config.Configuration;

public class FileUtil {
	private static final Logger LOGGER = Logger.getLogger(FileUtil.class.getName());

	public static <K, V> boolean writeToFile(String fileName, Map<K, V> map) {
		try {
			long startTime = System.currentTimeMillis();
			File file = new File(fileName);
			if(!file.exists()) {
			    file.createNewFile(); 
			}
			FileOutputStream fos = new FileOutputStream(fileName); 
			ObjectOutputStream out = new ObjectOutputStream(fos);   
			out.writeObject((Map<K, V>) map); 	
			out.writeObject(new EOFIndicator());
			out.close(); 
			fos.close(); 
			LOGGER.info("Time taken to write to disk : " + MemoryUtil.getTimeTaken(startTime) + "s");
			return true;
		} catch(FileNotFoundException e) {
			LOGGER.log(Level.WARNING, "File Not Found: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.log(Level.INFO, "IO Exception: " + e.getMessage());
			e.printStackTrace();
		}

		return false;
	}

	public static <K, V> Map<K, V> readFromFile(String fileName) {
		Map<K, V> data = null;
		try {
			long startTime = System.currentTimeMillis();
			File file = new File(fileName);
			if(!file.exists()) {
				return null;
			}
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			data = null;
			Object obj = null;
			while(!((obj = ois.readObject()) instanceof EOFIndicator)) {
				data = (Map<K, V>) obj;
			};
			ois.close();
			fis.close();
			LOGGER.info("Time taken to read from disk : " + MemoryUtil.getTimeTaken(startTime) + "s");
		} catch(EOFException e) {
			LOGGER.info("EOF Exception: " + e.getMessage());
		} catch(FileNotFoundException e) {
			LOGGER.log(Level.WARNING, "File Not Found: " + e.getMessage());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			LOGGER.log(Level.WARNING, "Class Not Found: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.log(Level.INFO, "IO Exception: " + e.getMessage());
			e.printStackTrace();
		}

		return data;
	}
	
	public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        return file.delete();        
	}

	public static long getFileSize(String filePath) {
		if(filePath == null) {
			return 0;
		}

		return new File(filePath).length();
	}

	public static String getProperFilePath(String filePath) {
		if(filePath == null) {
			return null;
		}

		return filePath.trim().charAt(filePath.length()-1) == '/' ? filePath : filePath + "/";
	}
	
	/**
	 * Gets the Shard Map's index File Name.
	 * 
	 * @param config
	 * @return Index filename of the Shard Map.
	 */
	public static String getIndexFileName(Configuration config) {
		return config.getFileOptions().getIndexFilePath() + config.getFileOptions().getIndexFileName();
	}

	public static String getIndexBackupFileName(Configuration config) {
		return config.getFileOptions().getBackupFilePath() + config.getFileOptions().getIndexFileName() + ".backup";
	}

	/**
	 * Get the master file name of the current shard which contains the serialized map.
	 * 
	 * @param config
	 * @return Master filename of the current shard.
	 */
	public static String getMasterFileName(Configuration config) {
		return getMasterFileName(config, config.getCurrentShardNum());
	}

	public static String getMasterFileName(Configuration config, int shard) {
		return config.getFileOptions().getMasterFilePath() + config.getFileOptions().getMasterFileName() + (shard > 0 ?  shard : "");
	}

	/**
	 * Get the backup file name of the current shard.
	 * 
	 * @param config
	 * @return Backup filename of the current shard
	 */
	public static String getBackupFileName(Configuration config) {
		return getBackupFileName(config, config.getCurrentShardNum());
	}

	public static String getBackupFileName(Configuration config, int shard) {
		return config.getFileOptions().getBackupFilePath() + config.getFileOptions().getMasterFileName() + (shard > 0 ?  shard : "") + ".backup";
	}
}
