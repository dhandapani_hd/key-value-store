package keyvaluestore.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import keyvaluestore.KeyValueStoreImpl;
import keyvaluestore.config.Configuration;

public class SerializationUtil {
	private static final Logger LOGGER = Logger.getLogger(KeyValueStoreImpl.class.getName());
	private static final int MB = 100 * 100;
	private static Map<Integer, Set<Integer>> shardMap = new ConcurrentHashMap<Integer, Set<Integer>>();
	//private static ReentrantReadWriteLock READ_WRITE_LOCK = new ReentrantReadWriteLock();

	/**
	 * Store the map to disk as a file and does necessary sharding to avoid memory exceptions during de-serialization.
	 * 
	 * @param map
	 * @param config
	 */
	public static <K, V> void serialize(Map<K, V> map, Configuration config) {
		//READ_WRITE_LOCK.writeLock().lock();
		try {
			int match = updateShards(map, config);
			//If Map is empty, no need of serialization or de-serialization
			if(match != map.size()) {
				//If shard size is within limit, append to the exising shard
				if(FileUtil.getFileSize(FileUtil.getMasterFileName(config)) <= config.getFileOptions().getMaxFileSize() * MB) {
					Map<K, V> diskMap = deSerialize(config, config.getCurrentShardNum());
					if(diskMap == null) {
						diskMap = new HashMap<>();
					}

					map.putAll(diskMap);
				}
				//Current file size exceeds the size limit, new shard will be created.
				else {
					config.setCurrentShardNum(config.getCurrentShardNum() + 1);
					LOGGER.info("Max File size reached. Creating a new file: " + FileUtil.getMasterFileName(config));	
				}

				populateShardMap(map, config);
				writeToFile(FileUtil.getIndexFileName(config), FileUtil.getIndexBackupFileName(config), shardMap, config);
				writeToFile(FileUtil.getMasterFileName(config), FileUtil.getBackupFileName(config), map, config);
				map.clear();
				System.gc();
			}
		}
		finally{
//			READ_WRITE_LOCK.writeLock().unlock();
		}
	}

	/**
	 * Finds the right file by looking up the index file using the key's hashcode and returns the deserialized object if matches else return null.
	 * 
	 * @param config
	 * @param key
	 * @return null if the key is not present in any shard and deserialized map if the key matches any shard.
	 */
	public static <K, V> Map<K, V> deSerialize(Configuration config, K key){
		for(Integer shard : shardMap.keySet()) {
			if(shardMap.get(shard).contains(key != null? key.hashCode() : 0)) {
				return deSerialize(config, shard);
			}
		}

		return null;
	}

	/**
	 * Return the de-serialized map from the given shard. If the master is deleted or corrupted, checks for valid backup files.
	 * 
	 * @param config
	 * @param shard
	 * @return
	 */
	public static <K, V> Map<K, V> deSerialize(Configuration config, Integer shard){
		Map<K, V> map = FileUtil.readFromFile(FileUtil.getMasterFileName(config, shard));
		if(map == null) {
			map = ReplicationUtil.getValidReplica(config, FileUtil.getBackupFileName(config));
		}

		return map;
	}

	/**
	 * Update the existing shards with new values from the current map if match.
	 * 
	 * @param map
	 * @param config
	 */
	public static <K, V> int updateShards(Map<K, V> map, Configuration config) {
		int match = 0;
		for(Integer key : shardMap.keySet()) {
			Set<Integer> value = shardMap.get(key);
			//LOGGER.info(value + " Map: " + map + " Thread: " + Thread.currentThread().getName());
			Map<K, V> diskMap = null;
			for(Entry<K, V> entry : map.entrySet()) {
				int actualValue = entry.getKey() != null? entry.getKey().hashCode() : 0;
				//If the shard contains the hashCode
				if(value != null && value.contains(actualValue)){
					match++;
					//Update the shard with the latest value
					if(diskMap == null) {
						diskMap = deSerialize(config, key);
					}
					if(diskMap == null) {
						diskMap = new HashMap<>();
					}
					diskMap.put(entry.getKey(), entry.getValue());
				}
			}
			
			writeToFile(FileUtil.getMasterFileName(config, key), FileUtil.getBackupFileName(config, key), diskMap, config);
		}
		
		LOGGER.info("Match: " + match + " Map: " + map);
		return match;
	}

	/**
	 * Populate Shard Map with the in memory map to store the hashcodes of the keys to the shard num.
	 * 
	 * @param map
	 * @param shardNum
	 */
	public static <K, V> void populateShardMap(Map<K, V> map, Configuration config) {
		for(Entry<K, V> entry : map.entrySet()) {
			Set<Integer> value = shardMap.get(config.getCurrentShardNum());
			if(value == null) {
				value = new HashSet<>();
			}
			value.add(entry.getKey() != null? entry.getKey().hashCode() : 0);
			shardMap.put(config.getCurrentShardNum(), value);
		}
	}

	/**
	 * Write the map to file and create replica
	 * 
	 * @param fileName
	 * @param backupFileName
	 * @param map
	 * @param config
	 */
	public static <K, V> void writeToFile(String fileName, String backupFileName, Map<K, V> map, Configuration config) {
		if(map != null && !map.isEmpty()) {
			FileUtil.deleteFile(fileName);
			FileUtil.writeToFile(fileName, map);
			FileUtil.deleteFile(backupFileName);
			ReplicationUtil.createReplica(map, config, backupFileName);
		}
	}
}
