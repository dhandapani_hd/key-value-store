package keyvaluestore;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import keyvaluestore.config.Configuration;
import keyvaluestore.utils.MemoryUtil;
import keyvaluestore.utils.SerializationUtil;

public class KeyValueStoreImpl<K extends Serializable, V extends Serializable> implements IKeyValueStore<K, V>{
	private static final Logger LOGGER = Logger.getLogger(KeyValueStoreImpl.class.getName());
	private Map<K, V> map = new HashMap<K, V>();
	private Configuration config;
	private final Object lock = new Object();

	public KeyValueStoreImpl() {
		init(new Configuration());
	}

	public KeyValueStoreImpl(Configuration config) {
		init(config);
	}

	private void init(Configuration config) {
		this.config = config;
	}

	private boolean containsKey(K key) {
		return map.get(key) != null;
	}

	@Override
	public V get(K key) {
		if(containsKey(key)) {
			return map.get(key);
		}else {
			//LOGGER.info("Key Not Found in Memory. De-Serialization Begins..");
			try{
				Map<K, V> diskMap = SerializationUtil.deSerialize(this.config, key);
				if(diskMap != null && diskMap.get(key) != null) {
					return diskMap.get(key);
				}
				else {
					LOGGER.info("Map: "+ diskMap +"Key Not Found: " + key + "(" + key.hashCode() + ")");
					return null;
				}
			}catch(OutOfMemoryError e) {
				LOGGER.log(Level.SEVERE, "Out of Memory Error: " + e.getMessage());
				return null;
			}
		}
	}	

	@Override
	public void put(K key, V value) {	
		synchronized (lock) {
			try {
				map.put(key, value);
				int memoryUsed = MemoryUtil.getUsedMemoryPercent();
				if(memoryUsed > config.getMemoryOptions().getMaxMemoryPercent()) {
					//LOGGER.info("Used Memory Percent Exceeded "+ memoryUsed +"%. Serialization in progress.");				
					SerializationUtil.serialize(map, this.config);
				}
			}catch(OutOfMemoryError e) {
				LOGGER.log(Level.SEVERE, "Out of Memory Error: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public Configuration getConfig() {
		return config;
	}
}
