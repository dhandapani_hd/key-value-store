package keyvaluestore.tests.util;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;

import keyvaluestore.KeyValueStoreImpl;
import keyvaluestore.tests.model.Company;
import keyvaluestore.tests.model.Employee;
import keyvaluestore.tests.model.Pair;
import keyvaluestore.utils.FileUtil;

public class TestUtil {
	/**
	 * Returns an Empty Key Value Store.
	 * 
	 * @return
	 */
	public static KeyValueStoreImpl<Serializable, Serializable> getEmptyMap() {
		return new KeyValueStoreImpl<Serializable, Serializable>();
	}
	
	/**
	 * Generates Random Key values pairs with random keys and random values
	 * 
	 * @return
	 */
	public static Pair<Serializable, Serializable> getRandomKeyValuePair() {
		int randValue = new Random().nextInt(100);
		int rand = new Random().nextInt(Integer.MAX_VALUE);
		
		if(randValue < 25) {
			//return new Pair<Serializable, Serializable>("inMobi", "dhandapani");
		}else if(randValue < 50) {
			return new Pair<Serializable, Serializable>(new Employee(rand + ""), 23);
		}else if(randValue < 75) {
			return new Pair<Serializable, Serializable>(new Pair<Serializable, Serializable>("dhandapani" + rand, "inMobi"), rand);
		}
		
		return new Pair<Serializable, Serializable>(new Company("inMobi" + randValue, Arrays.asList(new Employee("dhandapani" + rand))), "Software Engineer - Glance");
	}
	
	public static void clearData(KeyValueStoreImpl<Serializable, Serializable> store) {
		FileUtil.deleteFile(FileUtil.getIndexFileName(store.getConfig()));
		FileUtil.deleteFile(FileUtil.getIndexBackupFileName(store.getConfig()));

		int noOfShards = store.getConfig().getCurrentShardNum();
		while(noOfShards >= 0) {
			int backups = store.getConfig().getFileOptions().getMaxBackups();
			while(backups >= 0) {
				FileUtil.deleteFile(FileUtil.getBackupFileName(store.getConfig()) + (backups-1));
				backups--;
			}
			
			FileUtil.deleteFile(FileUtil.getMasterFileName(store.getConfig()));
			noOfShards--;
		}
		
		store = getEmptyMap();
	}

}
