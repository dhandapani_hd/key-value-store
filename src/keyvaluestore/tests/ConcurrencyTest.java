package keyvaluestore.tests;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.testng.Assert;
import org.testng.annotations.Test;

import keyvaluestore.KeyValueStoreImpl;
import keyvaluestore.tests.model.Pair;
import keyvaluestore.tests.util.TestUtil;

public class ConcurrencyTest {
	private KeyValueStoreImpl<Serializable, Serializable> store = TestUtil.getEmptyMap();
	private static int MAX_THREAD_SIZE = 1000;
	ExecutorService taskExecutor = Executors.newFixedThreadPool(MAX_THREAD_SIZE);
	CountDownLatch latch = new CountDownLatch(MAX_THREAD_SIZE);

	@Test()
	public void testPut() {
		TestUtil.clearData(store);
		store.getConfig().getMemoryOptions().setMaxMemoryPercent(20);
		store.getConfig().getFileOptions().setMaxFileSize(1);
		List<Callable<Boolean>> callables = new ArrayList<>();
		int i = 0;
		while(i++ < MAX_THREAD_SIZE) {
			Pair<Serializable, Serializable> keyValue = TestUtil.getRandomKeyValuePair();
			callables.add(new Callable<Boolean>() {
				@Override
				public Boolean call() throws Exception {
					store.put(keyValue.getFirst(), keyValue.getSecond());
					return keyValue.getSecond().equals(store.get(keyValue.getFirst()));
				}
			});
		}

		try {
			List<Future<Boolean>> futures = taskExecutor.invokeAll(callables);
			for(Future<Boolean> future : futures) {
				try {
					Assert.assertTrue(future.get());
				} catch (InterruptedException e) {	
					Thread.currentThread().interrupt();
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
		} catch (InterruptedException e1) {
			Thread.currentThread().interrupt();
		}
	}  
	
	@Test()
	public void testPutRandomOrder() {
		TestUtil.clearData(store);
		store.getConfig().getMemoryOptions().setMaxMemoryPercent(20);
		store.getConfig().getFileOptions().setMaxFileSize(5);
		List<Callable<Boolean>> callables = new ArrayList<>();
		int i = 0;
		while(i++ < MAX_THREAD_SIZE) {
			Pair<Serializable, Serializable> keyValue = TestUtil.getRandomKeyValuePair();
			callables.add(new Callable<Boolean>() {
				@Override
				public Boolean call() throws Exception {
					Thread.sleep(new Random().nextInt(2000));
					store.put(keyValue.getFirst(), keyValue.getSecond());
					return keyValue.getSecond().equals(store.get(keyValue.getFirst()));
				}
			});
		}

		try {
			List<Future<Boolean>> futures = taskExecutor.invokeAll(callables);
			for(Future<Boolean> future : futures) {
				try {
					Assert.assertTrue(future.get());
				} catch (InterruptedException e) {	
					Thread.currentThread().interrupt();
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
		} catch (InterruptedException e1) {
			Thread.currentThread().interrupt();
		}
	}  

	@Test()
	public void testShard() {
		TestUtil.clearData(store);
		int i = 0;
		store.getConfig().getMemoryOptions().setMaxMemoryPercent(0);
		store.getConfig().getFileOptions().setMaxFileSize(-1);
		store.getConfig().setCurrentShardNum(0);
		Pair<Serializable, Serializable> keyValue = TestUtil.getRandomKeyValuePair();
		
		while(i++ < MAX_THREAD_SIZE) {
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {					
					store.put(keyValue.getFirst(), keyValue.getSecond());
					latch.countDown();
				}
			});
			thread.start();
		}

		try {
			latch.await();
			Assert.assertTrue(store.getConfig().getCurrentShardNum() == 1); 
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			e.printStackTrace();
		}
	}
}
