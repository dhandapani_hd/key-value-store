package keyvaluestore.tests.model;

import java.io.Serializable;

public class Employee implements Serializable{
	private String name;
	public Employee(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		Employee object = (Employee) obj;
		if(object == null) {
			return false;
		}
		
		if((this.getName() != null && object.getName() == null) && (this.getName() == null && object.getName() != null) && !this.getName().equalsIgnoreCase(object.getName())) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int hashCode = 17;
		hashCode += 11 * (this.name != null? this.name.hashCode() : 0);
		return hashCode;
	}
	
}
