package keyvaluestore.tests.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Company implements Serializable {
	private String name;
	private List<Employee> employees;
	
	public Company(String name) {
		this.name = name;
	}
	
	public Company(String name, List<Employee> emps) {
		this.name = name;
		this.employees = emps;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	public void addEmployee(Employee emp) {
		if(this.employees == null) {
			this.employees = new ArrayList<>();
		}
		
		this.employees.add(emp);
	}
	
	@Override
	public boolean equals(Object obj) {
		Company object = (Company) obj;
		if(object == null) {
			return false;
		}
		
		if((this.getName() != null && object.getName() == null) && (this.getName() == null && object.getName() != null) && !this.getName().equalsIgnoreCase(object.getName())) {
			return false;
		}
		
		if((this.getEmployees() != null && object.getEmployees() == null) && (this.getEmployees() != null && object.getEmployees() == null)) {
			return false;
		}
		
		for(Employee emp : object.getEmployees()) {
			if(!this.employees.contains(emp)) {
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int hashCode = 19;
		hashCode += 11 * (this.name != null? this.name.hashCode() : 0);
		hashCode += 13 * (this.employees != null? this.employees.hashCode() : 0);
		
		return hashCode;
	}	
}
