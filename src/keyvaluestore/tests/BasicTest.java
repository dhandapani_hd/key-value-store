package keyvaluestore.tests;

import java.io.Serializable;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import keyvaluestore.KeyValueStoreImpl;
import keyvaluestore.tests.model.Pair;
import keyvaluestore.tests.util.TestUtil;
import keyvaluestore.utils.FileUtil;
import keyvaluestore.utils.StringUtil;

public class BasicTest{

	private KeyValueStoreImpl<Serializable, Serializable> store = TestUtil.getEmptyMap();

	@BeforeClass
	public void setUp() {
	}

	@Test()
	public void testInvalidGet() {
		TestUtil.clearData(store);
		Serializable value = store.get(-100);
		Assert.assertNull(value, StringUtil.getExpectedActualStr(value, null));
	}
	
	@Test()
	public void testPut() {
		TestUtil.clearData(store);
		store.getConfig().getMemoryOptions().setMaxMemoryPercent(75);
		Pair<Serializable, Serializable> keyValue = TestUtil.getRandomKeyValuePair();
		store.put(keyValue.getFirst(), keyValue.getSecond());
		Serializable value = store.get(keyValue.getFirst());
		Assert.assertTrue(keyValue.getSecond().equals(value), StringUtil.getExpectedActualStr(keyValue.getSecond(), value));
	}
	
	@Test()
	public void testDiskBackedGet() {
		TestUtil.clearData(store);
		store.getConfig().getMemoryOptions().setMaxMemoryPercent(0);
		Pair<Serializable, Serializable> keyValue = TestUtil.getRandomKeyValuePair();
		store.put(keyValue.getFirst(), keyValue.getSecond());
		store = TestUtil.getEmptyMap();
		Serializable value = store.get(keyValue.getFirst());
		Assert.assertTrue(keyValue.getSecond().equals(value), StringUtil.getExpectedActualStr(keyValue.getSecond(), value));
	}
	
	@Test()
	public void testReplicaGet() {
		TestUtil.clearData(store);
		store.getConfig().getMemoryOptions().setMaxMemoryPercent(0);
		Pair<Serializable, Serializable> keyValue = TestUtil.getRandomKeyValuePair();
		store.put(keyValue.getFirst(), keyValue.getSecond());
		store = TestUtil.getEmptyMap();
		Assert.assertTrue(FileUtil.deleteFile(FileUtil.getMasterFileName(store.getConfig())), "File Deletion Failed");
		Serializable value = store.get(keyValue.getFirst());
		Assert.assertTrue(keyValue.getSecond().equals(value), StringUtil.getExpectedActualStr(keyValue.getSecond(), value));
	}
	
	@Test()
	public void testShard() {
		TestUtil.clearData(store);
		store.getConfig().getMemoryOptions().setMaxMemoryPercent(0);
		store.getConfig().getFileOptions().setMaxFileSize(0);
		store.getConfig().setCurrentShardNum(0);
		Pair<Serializable, Serializable> pair1 = TestUtil.getRandomKeyValuePair();
		int rand = new Random().nextInt(100);
		
		store.put(rand, pair1.getSecond());
		store.put(rand+1, pair1.getSecond());
		store.put(rand+2, pair1.getSecond());
		
		Assert.assertTrue(store.getConfig().getCurrentShardNum() == 2);
		Assert.assertTrue(pair1.getSecond().equals(store.get(rand)));
		Assert.assertTrue(pair1.getSecond().equals(store.get(rand+1)));
		Assert.assertTrue(pair1.getSecond().equals(store.get(rand+2)));
	}
}
