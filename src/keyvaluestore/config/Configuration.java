package keyvaluestore.config;

public class Configuration {
	private FileOptions fileOptions = new FileOptions();
	private MemoryOptions memoryOptions = new MemoryOptions();
	private int currentShardNum = 0;

	public int getCurrentShardNum() {
		return currentShardNum;
	}

	public void setCurrentShardNum(int currentFileNum) {
		this.currentShardNum = currentFileNum;
	}
	
	public FileOptions getFileOptions() {
		return fileOptions;
	}
	
	public void setFileOptions(FileOptions fileOptions) {
		this.fileOptions = fileOptions;
	}
	
	public MemoryOptions getMemoryOptions() {
		return memoryOptions;
	}
	
	public void setMemoryOptions(MemoryOptions memoryOptions) {
		this.memoryOptions = memoryOptions;
	}
}
