package keyvaluestore.config;

import keyvaluestore.utils.FileUtil;

public class FileOptions {
	private int maxBackups = 1;
	private String backupFilePath = "shards/"; //System.getProperty("user.home") + File.pathSeparator;
	private String masterFilePath = "shards/";
	private String masterFileName = "master.ser";
	private String indexFilePath = "shards/";
	private String indexFileName = "index.ser";
	private int maxFileSize = 1;

	public String getIndexFilePath() {
		return indexFilePath;
	}

	public void setIndexFilePath(String indexFilePath) {
		this.indexFilePath = indexFilePath;
	}

	public String getIndexFileName() {
		return indexFileName;
	}

	public void setIndexFileName(String indexFileName) {
		this.indexFileName = indexFileName;
	}

	public String getMasterFilePath() {
		return masterFilePath;
	}

	public void setMasterFilePath(String masterFilePath) {
		this.masterFilePath = FileUtil.getProperFilePath(masterFilePath);
	}

	public String getMasterFileName() {
		return masterFileName;
	}

	public void setMasterFileName(String masterFileName) {
		this.masterFileName = masterFileName;
	}
	
	public int getMaxBackups() {
		return maxBackups;
	}

	public void setMaxBackups(int maxBackups) {
		this.maxBackups = maxBackups;
	}

	public String getBackupFilePath() {
		return backupFilePath;
	}

	public void setBackupFilePath(String backupFilePath) {
		this.backupFilePath = FileUtil.getProperFilePath(backupFilePath);
	}

	public int getMaxFileSize() {
		return maxFileSize;
	}

	public void setMaxFileSize(int maxFileSize) {
		this.maxFileSize = maxFileSize;
	}
}
