package keyvaluestore.config;

public class MemoryOptions {
	private int maxMemoryPercent = 70;

	public int getMaxMemoryPercent() {
		return maxMemoryPercent;
	}

	public void setMaxMemoryPercent(int maxMemoryPercent) {
		if(maxMemoryPercent > 100 || maxMemoryPercent < 0) {
			maxMemoryPercent = 70;
		}
		
		this.maxMemoryPercent = maxMemoryPercent;
	}
}
