## Key Value Store

A Simple generic Disk Backed, In-memory Key Value Store.

## Overview
A multi-threaded java application that stores and returns generic key-value pairs. The store can be instantiated with configurations such as

Memory Configurations:

* Maximum Memory percentage to be allocated.

Sharding Options:

* Maximum File Size
* Maximum backups for a shard
* Master and Backup shards names and path.

Once the store is created, get and put methods can be called n number of times. 

## Design
PUT:

* If the predefined memory percentage is not exceeded, the value is put in the map.
* If the memory exceeds, then two cases
	* Key is already present, we need to update the existing shard by looking up the shard index map.
	* Key is not present, put it in to new file(shard) and update the shard index map.
* The configured amount of backups are taken for each shards.

GET:

* If the key is present in in-memory map, return it.
* If the key in not present, then lookup the shard index map to find the shard and return the map from the shard.
* If the Shard is corrupted/deleted, then backup files are used to get the value.
* If none of the 

## Complexity
GET:

* Best Case: O(1)
* Average and Worst Case: O(Time for De-Serialization).

PUT:

* Best Case - O(1)
* Average and Worst Case: O(Time for Serialization).

## Running the tests
Install TestNG plugin for eclipse and provide the relative path '/key-value-store/src/keyvaluestore/tests/KeyValueStoreTests.xml'

The following cases have been covered
Basic Test: (Single Thread)

* Invalid Get - Get on a store where no data is available
* Put and Get - Standard Put and Get operation.
* Get from Shard - Get the key from the Shard files 
* Get from Replica - If Master shard is deleted/corrupted, data is retrieved from backups.
* Test Shard - Check if the same key is always updating the existing shard and not creating new one.

Concurrency Test: (Number of Threads to be configured in the test file)

* Put and Get - FIFO
* Put and Get - Random Order
* Sharding - Check if the same key is always updating the existing shard and not creating new one.

## Reports
The reports are provided in the reports folder.

## Alternate Work 
Repo - https://bitbucket.org/dhandapani_hd/key-value-store-concurrent-hashmap/src/master/

* Tried Concurrent hashmap for multi-thread put method
* Slower than the current branch hence went with single-threaded put.

## Future Work/Enhancements
* Logic for LRU, LFU etc. during put and get calls. (Strategy Pattern can be used).
* Improve serialization and de-serialization performance by using tree data structures.